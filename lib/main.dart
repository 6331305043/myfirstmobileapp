import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Local Deal';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 2;
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Home',
    ),
    Text(
      'Location',
    ),
    Text(
      '',
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text(
          'Local Deal',
          style: TextStyle(color: Colors.black),
        )),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: [
          searchBox(),
          divider(),
          deal(),
          divider(),
          prodlist1(),
          prodlist2(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: null,
        tooltip: 'Add new post',
        child: const Icon(Icons.add, color: Colors.white),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.green,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: 'Location',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.price_check),
            label: 'Deal',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}

Widget searchBox() {
  return Container(
    margin: const EdgeInsets.all(20),
    padding: const EdgeInsets.all(10),
    alignment: Alignment.centerLeft,
    decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color: Colors.black26, // Set border color
            width: 3.0), // Set border width
        borderRadius: const BorderRadius.all(
            Radius.circular(10.0)), // Set rounded corner radius
        boxShadow: const [
          BoxShadow(blurRadius: 10, color: Colors.black26, offset: Offset(1, 3))
        ] // Make rounded corner of border
        ),
    child: const Text("Type here to search"),
  );
}

Widget deal() => Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          'Favourite Deal',
          style: TextStyle(
            color: Colors.grey,
          ),
        ),
        Text(
          'My Deal',
          style: TextStyle(
            color: Colors.green,
          ),
        )
      ],
    );

Widget divider() => Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Divider(
            indent: 20.0,
            endIndent: 10.0,
            thickness: 1,
          ),
        ),
      ],
    );

Widget prodlist1() {
  return Container(
    padding: const EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Image.asset('images/img1.jpg', width: 140, height: 140),
        Column(
          children: [
            Text('Nokia E6 specs\n'),
            Text('750THB', style: TextStyle(fontSize: 18.0)),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.favorite, color: Colors.green),
                  tooltip: '1.5k',
                  onPressed: () {},
                ),
                Text('1.5k', style: TextStyle(color: Colors.black)),
                IconButton(
                  icon: const Icon(Icons.message_outlined, color: Colors.black),
                  tooltip: '250',
                  onPressed: () {},
                ),
                Text('250', style: TextStyle(color: Colors.black)),
              ],
            )
          ],
        ),
      ],
    ),
  );
}

Widget prodlist2() {
  return Container(
    padding: const EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Image.asset('images/img2.jpg', width: 140, height: 140),
        Column(
          children: [
            Text('VOOPOO X217 Vape\n'),
            Text('540THB', style: TextStyle(fontSize: 18.0)),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.favorite, color: Colors.green),
                  tooltip: '1k',
                  onPressed: () {},
                ),
                Text('1k', style: TextStyle(color: Colors.black)),
                IconButton(
                  icon: const Icon(Icons.message_outlined, color: Colors.black),
                  tooltip: '222',
                  onPressed: () {},
                ),
                Text('222', style: TextStyle(color: Colors.black)),
              ],
            )
          ],
        ),
      ],
    ),
  );
}
